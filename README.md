# Car Catalog Aggregator

## Требования к Software

- OPENJDK 11

## Стек технологий

- Spring Boot MVC

- Spring Boot JPA

- Liquibase

- PostgreSQL

- Apache Maven

## Запуск приложения

Для запуска приложения, необходимо на сервере запустить car-catalog-aggregator.bat.

## Endpoints

### Агрегатор автомобилей

http://185.89.10.104:8005/


