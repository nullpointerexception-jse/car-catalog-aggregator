package ru.nlmk.carcatalogaggregator.api.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import ru.nlmk.carcatalogaggregator.model.apimodel.ExternalDataCarBrandWrapper;
import ru.nlmk.carcatalogaggregator.model.apimodel.ExternalDataCarModelWrapper;


@Slf4j
@Service
public class DataRestClient {

	private static final String BRAND_SUFFIX = "/brands";

	private static final String MODEL_SUFFIX = "/models";

	private final RestTemplate restTemplate;

	@Autowired
	public DataRestClient(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public ExternalDataCarBrandWrapper getBrand(String url) {
		try {
			ResponseEntity<ExternalDataCarBrandWrapper> response = restTemplate.getForEntity(url + BRAND_SUFFIX,
					ExternalDataCarBrandWrapper.class);
			return response.getBody();
		} catch (ResourceAccessException exception) {
			log.info("Service is not available by url {}", url);
			return null;
		}
	}

	public ExternalDataCarModelWrapper getModel(String url, long id) {
		try {
			ResponseEntity<ExternalDataCarModelWrapper> response = restTemplate
					.getForEntity(url + MODEL_SUFFIX + "/" + id, ExternalDataCarModelWrapper.class);
			return response.getBody();
		} catch (ResourceAccessException exception) {
			log.info("Service is not available by url {}", url);
			return null;
		}
	}

}
