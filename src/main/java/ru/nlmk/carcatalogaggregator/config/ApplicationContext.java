package ru.nlmk.carcatalogaggregator.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.List;

@Configuration
public class ApplicationContext {

    @Value("${service-endpoints}")
    private String serviceString;

    private static final long TIME_OUT = 3;

    @Bean
    public List<String> urlList() {
        return List.of(serviceString.split(";"));
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder()
                .setReadTimeout(Duration.ofSeconds(TIME_OUT))
                .setConnectTimeout(Duration.ofSeconds(TIME_OUT))
                .build();
    }

}
