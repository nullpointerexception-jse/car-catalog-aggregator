package ru.nlmk.carcatalogaggregator.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nlmk.carcatalogaggregator.dto.CarDto;
import ru.nlmk.carcatalogaggregator.dto.Response;
import ru.nlmk.carcatalogaggregator.enumerated.Status;
import ru.nlmk.carcatalogaggregator.model.CarBrand;
import ru.nlmk.carcatalogaggregator.service.CarAggregatorService;
import ru.nlmk.carcatalogaggregator.service.ExternalDataService;

import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE)
public class CarCatalogAggregatorController {

    private final CarAggregatorService carAggregatorService;

    private final ExternalDataService externalDataService;

    private final List<String> urlList;

    @Autowired
    public CarCatalogAggregatorController(CarAggregatorService carAggregatorService, ExternalDataService externalDataService,
                                          List<String> urlList) {
        this.carAggregatorService = carAggregatorService;
        this.externalDataService = externalDataService;
        this.urlList = urlList;
    }

    @GetMapping("/catalog")
    public List<CarDto> findAllCars() {
        Map<String, List<CarBrand>> carBrandMap = externalDataService.getListFromUrlList(urlList);
        if (carBrandMap.isEmpty()) {
            log.info("External services did not return the list of brands");
            return carAggregatorService.findAllCars();
        }
        Response response = carAggregatorService.addAllCars(carBrandMap);
        if (Status.ERROR.equals(response.getStatus())) {
            log.error(response.getMessage());
        }
        return carAggregatorService.findAllCars();
    }

}
