package ru.nlmk.carcatalogaggregator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String brandName;

    private String modelName;

}
