package ru.nlmk.carcatalogaggregator.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ru.nlmk.carcatalogaggregator.dto.CarDto;
import ru.nlmk.carcatalogaggregator.model.CarBrand;

import java.util.List;

@Mapper
public interface CarBrandMapper {

    CarBrandMapper INSTANCE = Mappers.getMapper(CarBrandMapper.class);

    @Mapping(source = "name", target = "brandName")
    @Mapping(target = "modelName", expression = "java(null)")
    CarDto carBrandToCarDto(CarBrand carBrand);

    List<CarDto> carBrandListToCarDtoList(List<CarBrand> carBrands);

}
