package ru.nlmk.carcatalogaggregator.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ru.nlmk.carcatalogaggregator.dto.CarDto;
import ru.nlmk.carcatalogaggregator.model.CarModel;

import java.util.List;

@Mapper
public interface CarModelMapper {

    CarModelMapper INSTANCE = Mappers.getMapper(CarModelMapper.class);

    @Mapping(source = "name", target = "modelName")
    @Mapping(source = "carBrand.name", target = "brandName")
    CarDto carModelToCarDto(CarModel carModel);

    List<CarDto> carModelListToCarDtoList(List<CarModel> carModels);

}
