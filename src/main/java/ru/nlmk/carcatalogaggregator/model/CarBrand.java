package ru.nlmk.carcatalogaggregator.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "car_brand", schema = "public")
public class CarBrand {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "car_brand_seq")
	@SequenceGenerator(name = "car_brand_seq", sequenceName = "car_brand_seq", schema = "public", allocationSize = 1)
	private Long id;

	@Column(nullable = false)
	private String name;

	@OneToMany(mappedBy = "carBrand", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	private List<CarModel> carModels;

	@ManyToMany(mappedBy = "carBrands", fetch = FetchType.LAZY)
    private Set<CarCatalog> carCatalogs;

}
