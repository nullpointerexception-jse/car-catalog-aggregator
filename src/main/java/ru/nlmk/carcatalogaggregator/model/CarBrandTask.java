package ru.nlmk.carcatalogaggregator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CarBrandTask {

	private String url;

	private List<CarBrand> carBrands;
}
