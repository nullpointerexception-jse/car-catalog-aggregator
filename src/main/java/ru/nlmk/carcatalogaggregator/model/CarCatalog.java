package ru.nlmk.carcatalogaggregator.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "car_catalog", schema = "public")
public class CarCatalog {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "car_catalog_seq")
    @SequenceGenerator(name = "car_catalog_seq", sequenceName = "car_catalog_seq", schema = "public", allocationSize = 1)
    private Long id;

    @Column(nullable = false)
    private String url;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "car_brand_catalog",
            joinColumns = { @JoinColumn(name = "catalog_id") },
            inverseJoinColumns = { @JoinColumn(name = "brand_id") }
    )
    private Set<CarBrand> carBrands;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "car_model_catalog",
            joinColumns = { @JoinColumn(name = "catalog_id") },
            inverseJoinColumns = { @JoinColumn(name = "model_id") }
    )
    private Set<CarModel> carModels;

}
