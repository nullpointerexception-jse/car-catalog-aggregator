package ru.nlmk.carcatalogaggregator.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "car_model", schema = "public")
public class CarModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "car_model_seq")
	@SequenceGenerator(name = "car_model_seq", sequenceName = "car_model_seq", schema = "public", allocationSize = 1)
	private Long id;

	@Column(nullable = false)
	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "brand_id", nullable = false)
	private CarBrand carBrand;

	@ManyToMany(mappedBy = "carModels", fetch = FetchType.LAZY)
	private Set<CarCatalog> carCatalogs;

}
