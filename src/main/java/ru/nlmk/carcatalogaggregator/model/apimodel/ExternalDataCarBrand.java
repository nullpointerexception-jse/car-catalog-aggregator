package ru.nlmk.carcatalogaggregator.model.apimodel;

import lombok.Data;

@Data
public class ExternalDataCarBrand {

	private Long id;

	private String name;

}
