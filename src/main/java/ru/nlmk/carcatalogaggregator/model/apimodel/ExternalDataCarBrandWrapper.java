package ru.nlmk.carcatalogaggregator.model.apimodel;

import lombok.Data;

import java.util.ArrayList;

@Data
public class ExternalDataCarBrandWrapper extends ArrayList<ExternalDataCarBrand> {

	/**
	 * Сгенерированный версионный UID сериализации
	 */
	private static final long serialVersionUID = -6666731001904493728L;

}
