package ru.nlmk.carcatalogaggregator.model.apimodel;

import lombok.Data;

@Data
public class ExternalDataCarModel {

	private Long id;

	private String name;

	private Long brandId;

}
