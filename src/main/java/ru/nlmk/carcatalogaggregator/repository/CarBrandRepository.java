package ru.nlmk.carcatalogaggregator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nlmk.carcatalogaggregator.model.CarBrand;

import java.util.List;
import java.util.Optional;

public interface CarBrandRepository extends JpaRepository<CarBrand, Long> {

    Optional<CarBrand> findByNameIgnoreCase(String name);

    List<CarBrand> findAllByCarCatalogsIsNotEmptyAndCarModelsIsEmpty();

}
