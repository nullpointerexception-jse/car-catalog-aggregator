package ru.nlmk.carcatalogaggregator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nlmk.carcatalogaggregator.model.CarCatalog;

import java.util.Optional;

public interface CarCatalogRepository extends JpaRepository<CarCatalog, Long> {

    Optional<CarCatalog> findByUrl(String url);

}
