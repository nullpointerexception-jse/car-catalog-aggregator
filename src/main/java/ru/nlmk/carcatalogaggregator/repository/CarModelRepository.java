package ru.nlmk.carcatalogaggregator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nlmk.carcatalogaggregator.model.CarModel;

import java.util.List;
import java.util.Optional;

public interface CarModelRepository extends JpaRepository<CarModel, Long> {

    Optional<CarModel> findByNameIgnoreCaseAndCarBrandId(String name, Long brandId);

    List<CarModel> findAllByCarCatalogsIsNotEmpty();

}
