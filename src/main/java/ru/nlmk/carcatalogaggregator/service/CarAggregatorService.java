package ru.nlmk.carcatalogaggregator.service;

import ru.nlmk.carcatalogaggregator.dto.CarDto;
import ru.nlmk.carcatalogaggregator.dto.Response;
import ru.nlmk.carcatalogaggregator.model.CarBrand;

import java.util.List;
import java.util.Map;

/**
 * Интерфейс сервиса добавления в БД и загрузки из БД данных автомобилей
 */
public interface CarAggregatorService {

    /**
     * Добавляет данные автомобилей в базу данных по {@link Map} со строкой URL в качестве ключа и списком объектов
     *   {@link List}<{@link CarBrand}> в качестве значения, возвращает {@link Response}
     *
     * @param urlCarBrandMap {@link Map} содержащая {@link String} строку URL в качестве ключа и список объектов
     *   {@link List}<{@link CarBrand}> в качестве значения
     * @return {@link Response} со статусом операции и, если произошла ошибка, сообщением об ошибке
     */
    Response addAllCars(Map<String, List<CarBrand>> urlCarBrandMap);

    /**
     * Загружает все данные автомобилей из базы данных и возвращает {@link List}<{@link CarDto}>
     *
     * @return {@link List}<{@link CarDto}> список данных автомобилей
     */
    List<CarDto> findAllCars();

}
