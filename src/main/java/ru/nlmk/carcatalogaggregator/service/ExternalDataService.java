package ru.nlmk.carcatalogaggregator.service;

import ru.nlmk.carcatalogaggregator.model.CarBrand;

import java.util.List;
import java.util.Map;

/**
 * Интерфейс сервиса получения данных <{@link CarBrand}> по URL источников
 *
 * @author potapov_vs
 */
public interface ExternalDataService {

	/**
	 * Метод для получения списка объектов {@link List}<{@link CarBrand}> по указанному URL.
	 * Возвращает {@code null}, если сервис по данному URL не отвечает,
	 * возвращает Пустой {@link List}<{@link CarBrand}>, если сервис по данному URL отвечает, но список брэндов пуст.
	 *
	 * @param url - URL источника данных
	 * @return <ul>
	 * <li>{@link List}<{@link CarBrand}> - список брендов
	 * <li>Пустой {@link List}<{@link CarBrand}> - если сервис по данному URL не отвечает или список брэндов пуст
	 * </ul>
	 */
	List<CarBrand> getDataFromUrl(String url);

	/**
	 * Метод для получения коллекции, содержащей {@link String}  строку URL в качестве ключа и список объектов
	 * {@link List}<{@link CarBrand}> в качестве значения, по {@link List}<{@link String}> списку URL источников данных.
	 *
	 * @param urlList - {@link List}<{@link String}> список URL источников данных
	 * @return {@link Map} содержащая {@link String}  строку URL в качестве ключа и список объектов
	 * {@link List}<{@link CarBrand}> в качестве значения.
	 */
	Map<String, List<CarBrand>> getListFromUrlList(List<String> urlList);

}
