package ru.nlmk.carcatalogaggregator.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nlmk.carcatalogaggregator.dto.CarDto;
import ru.nlmk.carcatalogaggregator.dto.Response;
import ru.nlmk.carcatalogaggregator.dto.mapper.CarBrandMapper;
import ru.nlmk.carcatalogaggregator.dto.mapper.CarModelMapper;
import ru.nlmk.carcatalogaggregator.enumerated.Status;
import ru.nlmk.carcatalogaggregator.model.CarBrand;
import ru.nlmk.carcatalogaggregator.model.CarCatalog;
import ru.nlmk.carcatalogaggregator.model.CarModel;
import ru.nlmk.carcatalogaggregator.repository.CarBrandRepository;
import ru.nlmk.carcatalogaggregator.repository.CarCatalogRepository;
import ru.nlmk.carcatalogaggregator.repository.CarModelRepository;
import ru.nlmk.carcatalogaggregator.service.CarAggregatorService;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Service
public class CarAggregatorServiceImpl implements CarAggregatorService {

    private static final CarBrandMapper carBrandMapper = CarBrandMapper.INSTANCE;

    private static final CarModelMapper carModelMapper = CarModelMapper.INSTANCE;

    private final CarBrandRepository carBrandRepository;

    private final CarModelRepository carModelRepository;

    private final CarCatalogRepository carCatalogRepository;

    @Autowired
    public CarAggregatorServiceImpl(CarBrandRepository carBrandRepository, CarModelRepository carModelRepository, CarCatalogRepository carCatalogRepository) {
        this.carBrandRepository = carBrandRepository;
        this.carModelRepository = carModelRepository;
        this.carCatalogRepository = carCatalogRepository;
    }

    @Override
    @Transactional
    public Response addAllCars(Map<String, List<CarBrand>> urlCarBrandMap) {
        if (urlCarBrandMap == null) {
            return Response.builder().status(Status.ERROR).message("Invalid data of cars").build();
        }

        for (Map.Entry<String, List<CarBrand>> entry: urlCarBrandMap.entrySet()) {
            final String url = entry.getKey();
            final List<CarBrand> externalBrands = entry.getValue();

            CarCatalog catalog = carCatalogRepository.findByUrl(url).orElseGet(() ->
                    carCatalogRepository.save(CarCatalog.builder()
                                                        .url(url)
                                                        .carBrands(new HashSet<>())
                                                        .carModels(new HashSet<>())
                                                        .build())
            );
            catalog.getCarBrands().clear();
            catalog.getCarModels().clear();

            for (CarBrand externalBrand : externalBrands) {
                CarBrand brand = carBrandRepository.findByNameIgnoreCase(externalBrand.getName()).orElseGet(() ->
                        carBrandRepository.save(CarBrand.builder().name(externalBrand.getName()).build())
                );
                catalog.getCarBrands().add(brand);

                for (CarModel externalModel : externalBrand.getCarModels()) {
                    CarModel model = carModelRepository.findByNameIgnoreCaseAndCarBrandId(externalModel.getName(), brand.getId())
                            .orElseGet(() ->
                                    carModelRepository.save(CarModel.builder()
                                            .name(externalModel.getName())
                                            .carBrand(brand)
                                            .build())
                            );
                    catalog.getCarModels().add(model);
                }
            }
        }

        return Response.builder().status(Status.OK).build();
    }

    @Override
    public List<CarDto> findAllCars() {
        List<CarDto> cars = carModelMapper.carModelListToCarDtoList(carModelRepository.findAllByCarCatalogsIsNotEmpty());
        List<CarDto> carsWithoutModels = carBrandMapper.carBrandListToCarDtoList(
                carBrandRepository.findAllByCarCatalogsIsNotEmptyAndCarModelsIsEmpty());
        cars.addAll(carsWithoutModels);
        cars.sort(Comparator.comparing(CarDto::getBrandName).thenComparing(CarDto::getModelName));
        return cars;
    }

}
