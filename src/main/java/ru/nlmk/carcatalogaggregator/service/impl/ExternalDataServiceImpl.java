package ru.nlmk.carcatalogaggregator.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nlmk.carcatalogaggregator.api.client.DataRestClient;
import ru.nlmk.carcatalogaggregator.model.CarBrand;
import ru.nlmk.carcatalogaggregator.model.CarBrandTask;
import ru.nlmk.carcatalogaggregator.model.CarModel;
import ru.nlmk.carcatalogaggregator.model.apimodel.ExternalDataCarBrand;
import ru.nlmk.carcatalogaggregator.model.apimodel.ExternalDataCarBrandWrapper;
import ru.nlmk.carcatalogaggregator.model.apimodel.ExternalDataCarModel;
import ru.nlmk.carcatalogaggregator.model.apimodel.ExternalDataCarModelWrapper;
import ru.nlmk.carcatalogaggregator.service.ExternalDataService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Slf4j
@Service
public class ExternalDataServiceImpl implements ExternalDataService {

	private final DataRestClient dataRestClient;

	@Autowired
	public ExternalDataServiceImpl(DataRestClient dataRestClient) {
		this.dataRestClient = dataRestClient;
	}

	@Override
	public List<CarBrand> getDataFromUrl(String url) {
		ExternalDataCarBrandWrapper externalDataCarBrandWrapper = dataRestClient.getBrand(url);
		if (externalDataCarBrandWrapper == null || externalDataCarBrandWrapper.isEmpty()) {
			return Collections.emptyList();
		}
		List<CarBrand> carBrands = new ArrayList<>();
		for (ExternalDataCarBrand externalDataCarBrand : externalDataCarBrandWrapper) {
			CarBrand carBrand = new CarBrand();
			List<CarModel> carModels = new ArrayList<>();
			carBrand.setId(externalDataCarBrand.getId());
			carBrand.setName(externalDataCarBrand.getName());
			ExternalDataCarModelWrapper externalDataCarModelWrapper = dataRestClient.getModel(url, externalDataCarBrand.getId());
			for (ExternalDataCarModel externalDataCarModel : externalDataCarModelWrapper) {
				CarModel carModel = new CarModel();
				carModel.setId(externalDataCarModel.getId());
				carModel.setName(externalDataCarModel.getName());
				carModel.setCarBrand(carBrand);
				carModels.add(carModel);
			}
			carBrand.setCarModels(carModels);
			carBrands.add(carBrand);
		}
		return carBrands;
	}

	@Override
	public Map<String, List<CarBrand>> getListFromUrlList(List<String> urlList) {
		Map<String, List<CarBrand>> urlMap = new HashMap<>();
		ExecutorService executorService = Executors.newCachedThreadPool();
		List<Callable<CarBrandTask>> tasks = new ArrayList<>();
		for (String url : urlList) {
			tasks.add(() -> {
				CarBrandTask carBrandTask = new CarBrandTask();
				carBrandTask.setUrl(url);
				carBrandTask.setCarBrands(getDataFromUrl(url));
				return carBrandTask;
			});
		}
		List<Future<CarBrandTask>> results = null;
		try {
			results = executorService.invokeAll(tasks);
		} catch (InterruptedException e) {
			log.error(e.getMessage());
			Thread.currentThread().interrupt();
		}
		if (results == null) {
			return urlMap;
		}
		for (Future<CarBrandTask> value : results) {
			try {
				CarBrandTask carBrandTask = value.get();
				urlMap.put(carBrandTask.getUrl(), carBrandTask.getCarBrands());
			} catch (InterruptedException | ExecutionException e) {
				log.error(e.getMessage());
				Thread.currentThread().interrupt();
			}
		}
		return urlMap;
	}

}
