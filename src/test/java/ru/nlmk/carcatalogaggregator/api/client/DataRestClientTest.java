package ru.nlmk.carcatalogaggregator.api.client;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import ru.nlmk.carcatalogaggregator.model.apimodel.ExternalDataCarBrandWrapper;
import ru.nlmk.carcatalogaggregator.model.apimodel.ExternalDataCarModelWrapper;

@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
class DataRestClientTest {

	@Autowired
	private DataRestClient dataRestClient;

	private static final String URL = "http://185.89.10.104:8004";

	private static final String WRONG_URL = "http://185.89.10.274:8004";

	@Test
	void getBrand() {
		ExternalDataCarBrandWrapper result = dataRestClient.getBrand(URL);
		assertNotNull(result);
	}

	@Test
	void getBrandWithWrongUrl() {
		ExternalDataCarBrandWrapper result = dataRestClient.getBrand(WRONG_URL);
		assertNull(result);
	}

	@Test
	void getModel() {
		ExternalDataCarModelWrapper result = dataRestClient.getModel(URL, 1);
		assertNotNull(result);
	}

}