package ru.nlmk.carcatalogaggregator.service.impl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import ru.nlmk.carcatalogaggregator.dto.CarDto;
import ru.nlmk.carcatalogaggregator.dto.Response;
import ru.nlmk.carcatalogaggregator.enumerated.Status;
import ru.nlmk.carcatalogaggregator.model.CarBrand;
import ru.nlmk.carcatalogaggregator.service.CarAggregatorService;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
public class CarAggregatorServiceImplTest {

    @Autowired
    private CarAggregatorService carAggregatorService;

    private static final String URL = "http://test";

    private static final String BRAND_NAME_1 = "Audi";
    private static final String BRAND_NAME_2 = "BMW";

    @Test
    void testAddAllCars() {
        Map<String, List<CarBrand>> carsMap = new HashMap<>();
        carsMap.put(URL, List.of(CarBrand.builder().name(BRAND_NAME_1).carModels(Collections.emptyList()).build(),
                CarBrand.builder().name(BRAND_NAME_2).carModels(Collections.emptyList()).build()));
        Response response = carAggregatorService.addAllCars(carsMap);
        List<CarDto> cars = carAggregatorService.findAllCars();
        assertEquals(Status.OK, response.getStatus());
        assertEquals(carsMap.get(URL).size(), cars.size());

        carsMap.put(URL, Collections.emptyList());
        response = carAggregatorService.addAllCars(carsMap);
        assertEquals(Status.OK, response.getStatus());
        assertEquals(0, carAggregatorService.findAllCars().size());
    }

    @Test
    void testAddAllCarsNull() {
        Response response = carAggregatorService.addAllCars(null);
        assertEquals(Status.ERROR, response.getStatus());
    }

    @Test
    void testFindAllCars() {
        List<CarDto> cars = carAggregatorService.findAllCars();
        assertNotNull(cars);
    }

}