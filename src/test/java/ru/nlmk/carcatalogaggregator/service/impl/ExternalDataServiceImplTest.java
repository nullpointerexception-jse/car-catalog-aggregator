package ru.nlmk.carcatalogaggregator.service.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import ru.nlmk.carcatalogaggregator.model.CarBrand;

@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
class ExternalDataServiceImplTest {

	private static final String URL_1 = "http://185.89.10.104:8004";
	private static final String URL_2 = "http://185.89.10.104:8004";
	private static final String WRONG_URL = "http://185.89.10.274:8004";
	@Autowired
	private ExternalDataServiceImpl externalDataService;

	@Test
	void getDataFromUrl() {
		List<CarBrand> result = externalDataService.getDataFromUrl(URL_1);
		assertNotNull(result);
	}

	@Test
	void getDataFromWrongUrl() {
		List<CarBrand> result = externalDataService.getDataFromUrl(WRONG_URL);
		System.out.println(result);
		assertTrue(result.isEmpty());
	}

	@Test
	void getListFromUrlList() {
		List<String> urlList = Stream.of(URL_1, URL_2).collect(Collectors.toList());
		Map<String, List<CarBrand>> result = externalDataService.getListFromUrlList(urlList);
		assertNotNull(result);
	}

	@Test
	void getListFromWrongUrlList() {
		List<String> urlList = Stream.of(URL_1, URL_2).collect(Collectors.toList());
		Map<String, List<CarBrand>> result = externalDataService.getListFromUrlList(urlList);
		assertNotNull(result);
	}

}